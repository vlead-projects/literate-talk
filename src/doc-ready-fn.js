Hooks.addHook('documentReady', 
              function() {
        $.each($("#outreach-status td").filter(".org-right"), 
               function(i, el) {
                   el.innerText = addCommas(el.innerText);});
    }
);

Hooks.addHook('documentReady', 
    function() {
	$("#date").text($(".postamble")[0].innerHTML);

// new Date().toLocaleString());
    }
);

Hooks.addHook('documentReady', 
    function() {
	$("a").attr("target", "_blank");
    }
);
    

Hooks.addHook('documentReady', 
    function() {
	$("#sec-title-slide > h1").addClass("no-toc-progress");
    }
);
    

$(document).ready(function () {Hooks.runHooks('documentReady');});
