<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Literate Programming in Emacs Org-mode</title>
<meta name="author" content="((link (:type span :path author :format bracket :raw-link span:author :application nil :search-option nil :begin 1 :end 79 :contents-begin 16 :contents-end 77 :post-blank 0 :parent #0) Venkatesh Choppella (export-snippet (:back-end latex :value \\ :begin 35 :end 48 :post-blank 1 :parent #1)) (export-snippet (:back-end html :value <br/> :begin 48 :end 63 :post-blank 1 :parent #1)) IIIT Hyderabad))"/>
<style type="text/css">
.underline { text-decoration: underline; }
</style>
<link rel="stylesheet" href="lib-git-ignore/reveal.js/css/reveal.css"/>

<link rel="stylesheet" href="lib-git-ignore/reveal.js/css/theme/white.css" id="theme"/>

<link rel="stylesheet" href="./local.css"/>

<!-- If the query includes 'print-pdf', include the PDF print sheet -->
<script>
    if( window.location.search.match( /print-pdf/gi ) ) {
        var link = document.createElement( 'link' );
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = 'lib-git-ignore/reveal.js/css/print/pdf.css';
        document.getElementsByTagName( 'head' )[0].appendChild( link );
    }
</script>
<meta name="description" content="Virtual Labs PRSG meeting IIT Delhi 2017-04-21.">
</head>
<body>
<div class="reveal">
<div class="slides">
<section id="sec-title-slide"><h1 class="title">Literate Programming in Emacs Org-mode</h1><h2 class="author"><span class="author">Venkatesh Choppella <br/> IIIT Hyderabad</span></h2><h2 class="date">2018-07-08 Sun 00:00</h2><p class="date">Created: 2018-07-08 Sun 09:55</p>
</section>
<div class="header-right"><img src="./vlabs-color-large.png" height="130"></img></div>
<!--    <div class="header-left"><img src="./vlead-color-large.png" height="130"></img></div> -->
<!--    <div class="footer-left"></div> -->
	 <script type="text/javascript" src="./numformat.js"></script>
	 <script type="text/javascript" src="./event-hooks.js"></script> 
	 <script type="text/javascript" src="./lib-git-ignore/jquery-2.2.3.min.js"></script> 
	 <script type="text/javascript" src="./doc-ready-fn.js"></script> 
<!--	 <div class="watermark">DRAFT of <span id="date"></span></div> -->



<section>
<section id="slide-org03810f7">
<h2 id="org03810f7">Abstract</h2>
<div class="outline-text-2" id="text-org03810f7">
</div>
</section>
<section id="slide-org58265b2">
<h3 id="org58265b2"></h3>
<p>
Literate Programming, invented by Knuth in the late 1970's,
is a style of programming that inverts the idea of program
documentation.  Rather than adding comments to your code,
you embed your code into a narrative consisting of natural
language, diagrams, charts and other literate artefacts.
</p>

<p>
Knuth used literate programming to describe the
implementation of the TeX typesetting program.
</p>

</section>
<section id="slide-org246db6c">
<h3 id="org246db6c"></h3>
<p>
Text Editors and other tools have finally caught up to
support literate programming.  In this talk, we demonstrate
literate programming in Emacs Org-mode, which allows
literate programming in multiple programming languages in
the same document.  We discuss case studies that demonstrate
the use of literate programming in Software Development,
DevOps, and teaching undergraduate computing students.
</p>

</section>
</section>
<section>
<section id="slide-orgc6a490a">
<h2 id="orgc6a490a">Story 1: A familiar programming problem</h2>
<div class="outline-text-2" id="text-orgc6a490a">
</div>
</section>
<section id="slide-org1265830">
<h3 id="org1265830">You know this</h3>
<pre class="example">
(define (! n)
  (if (= n 0)
      1
      (* n (! (sub1 n)))))

</pre>


</section>
<section id="slide-orgd38e8b7">
<h3 id="orgd38e8b7">How about this?</h3>
<pre class="example">
 (define ! (scons 1 (s* +ves !)))
</pre>

</section>
<section id="slide-org31c6ccd">
<h3 id="org31c6ccd">This might help a bit</h3>
<pre class="example">
   ;;; !   =                     [!0   !1   !2   !3 ...]
   ;;; +ves   =                  [1    2    3    4  ...]
   ;;; (s* +ves !) =             [!1   !2   !3   !4 ...]
   ;;; (scons 1  (s* +ves !)) =  [!0   !1   !2   !3 ...]
</pre>

</section>
<section id="slide-org8b0c11d">
<h3 id="org8b0c11d">Narrative could help much more</h3>
<div class="outline-text-3" id="text-org8b0c11d">
</div>
</section>
<section id="slide-org39d681d">
<h4 id="org39d681d">Streams are functions over natural numbers</h4>
<ul>
<li>We want to compute the factorial function <code>!</code>.</li>

<li>Note that <code>!</code> is a function over natural numbers</li>

</ul>

</section>
<section id="slide-orgb563e17">
<h4 id="orgb563e17">contd ..</h4>
<ul>
<li>Any function <code>f=over naturals may be thought of as an
   infinite list.  =f(n)</code> is simply the <code>n=th element of
   =f</code>.</li>

<li>An infinite list is a called a <i>stream</i>.</li>

</ul>

</section>
<section id="slide-org137984e">
<h4 id="org137984e">Factorial as a stream</h4>
<p>
<code>!</code> is a stream
</p>

<pre class="example">
! = [1  1  2  6  ...]
  = [0! 1! 2! 3! ...]
</pre>

</section>
<section id="slide-org37dea64">
<h4 id="org37dea64">Positives as a stream</h4>
<p>
We also have the stream of positive natural numbers:
</p>

<pre class="example">
+ves = [1  2  3  4  ...]
</pre>

</section>
<section id="slide-org550ba9e">
<h4 id="org550ba9e">Pairwise multiplication of streams</h4>
<p>
Pairwise multiplying the two infinite lists gives us
</p>

<pre class="example">
(s* +ves !) = [1*!0  2*!1  3*!2  4*!3 ...]
</pre>

<p>
But this is just
</p>
<pre class="example">
[!1 !2 !3 !4 ...]
</pre>

</section>
<section id="slide-orge498899">
<h4 id="orge498899">Stream cons adds an element to a stream</h4>
<p>
<code>[!1 !2 !3 !4 ...]</code> looks like the <code>!</code> function, except that
it is missing a leading <code>!0</code>.
</p>

<pre class="example">
(scons 1 (s* +ves !)) = [!0 !1 !2 !3 ...]
</pre>

</section>
<section id="slide-orgcbab88c">
<h4 id="orgcbab88c">A (co)recursive definition of <code>!</code></h4>
<p>
<code>!</code> satisfies the (co)recursive equation
</p>

<pre class="example">
! = (scons 1 (s* +ves !))
</pre>

</section>
<section id="slide-org198e6a0">
<h4 id="org198e6a0">Encoding the definition in Scheme</h4>
<p>
This can simply be turned into a definition
</p>

<pre class="example">
(define ! (scons 1 (s* +ves !)))
</pre>

</section>
</section>
<section>
<section id="slide-org050bef5">
<h2 id="org050bef5">Story 2: A familiar system design problem</h2>
<p>
Designing and commissioning a cluster on commercial cloud.
</p>

</section>
<section id="slide-orgb8cc50d">
<h3 id="orgb8cc50d">A common handicap</h3>
<ul>
<li>We do not train sys admins for software engineering.</li>

<li>As a result they do not think of sys administration as
fundamentally a software engineering activity.</li>

<li>They are unfamiliar with version control, documentation
standards, etc.</li>

</ul>

</section>
<section id="slide-orgb24039d">
<h3 id="orgb24039d">A (bad) way of doing things</h3>
<ul>
<li><i>Manually</i> configure several systems (DNS, Reverse
Proxy, Router, Lab VM, etc.).</li>

<li>The only copy of the configuration is on the machines
themselves.</li>

<li>Documentation of configuration on a wiki done post hoc.</li>

</ul>

</section>
<section id="slide-org33b111d">
<h3 id="org33b111d">You know what happens next &#x2026;</h3>

</section>
<section id="slide-orgbace53c">
<h3 id="orgbace53c">Where is the problem?</h3>
<ul>
<li>Too much of the design in the head of the sys admins and
sys engineers.  And some of it was wrong!</li>

<li>Configuration (code) lacks a narrative.</li>

<li>Where are the requirements?</li>

<li>Where is the network diagram?</li>

</ul>

</section>
<section id="slide-orge491915">
<h3 id="orge491915">Another (better) way:  Literate DevOps</h3>
<ul>
<li>Version control (git)</li>

<li>DSL for configuration (<code>yaml</code>)</li>

<li>Literate programming (tangle <code>yaml</code> and shell scripts out
of narrative)</li>

</ul>

</section>
<section id="slide-orgae2fb4d">
<h3 id="orgae2fb4d">contd ..</h3>
<ul>
<li>Requirements part of narrative</li>

<li>Design part of narrative  (n/w diagrams, etc.)</li>

</ul>

</section>
<section id="slide-org0aaa85e">
<h3 id="org0aaa85e">Example:  N/W diagram and firewall rules</h3>
<p target="_blank">
<a href="http://exp.vlabs.ac.in/presentations/sys-mod/common.html" target="_blank">Common Roles</a>
</p>

</section>
</section>
<section>
<section id="slide-org5b2653d">
<h2 id="org5b2653d">Story 3: Web Portal for Virtual Labs outreach activities</h2>
<p>
<a href="https://github.com/vlead/outreach-portal.git">https://github.com/vlead/outreach-portal.git</a>
</p>

</section>
</section>
<section>
<section id="slide-org396b1fd">
<h2 id="org396b1fd">contd ..</h2>
<ul>
<li>Requirements</li>
<li>Data Model</li>
<li>Code</li>
<li>Test Cases</li>

</ul>

</section>
</section>
<section>
<section id="slide-org3864a4b">
<h2 id="org3864a4b">Story 4: Teaching IT Workshop 2 @ IIIT Hyderabad</h2>
<ul>
<li><a href="http://pascal.iiit.ac.in/~itws2/docs">http://pascal.iiit.ac.in/~itws2/docs</a></li>
<li>Combines concept explanation and experiment.</li>

</ul>

</section>
</section>
<section>
<section id="slide-org4fe8be0">
<h2 id="org4fe8be0">contd ..</h2>
<ul>
<li>Lots of documentation related to process of running a
course.  Multiple repositories.  Lecture notes, course
pages, assignments, lab sheets and exams etc. given as
literate programs.  Assignments have test cases that
tangle out.  (Show final exam.)  Extensive documentation
helped all TAs and students to get on to the same page.
Generated HTML pages were hosted on the course server.</li>

</ul>


</section>
</section>
<section>
<section id="slide-org2d1cb95">
<h2 id="org2d1cb95">Conclusion: Literate programming is about Narratives</h2>
<div class="outline-text-2" id="text-org2d1cb95">
</div>
</section>
<section id="slide-org2f0089f">
<h3 id="org2f0089f">Programs need a narrative, not just comments</h3>
</section>
<section id="slide-org01b8fb8">
<h3 id="org01b8fb8">Comprehension needs redundancy</h3>
</section>
<section id="slide-org89c59c7">
<h3 id="org89c59c7">Narratives can be rich!</h3>
<ul>
<li>Formulas</li>
<li>Diagrams</li>
<li>animations.</li>

</ul>

</section>
</section>
<section>
<section id="slide-orga0605eb">
<h2 id="orga0605eb">Research:  Tools for Literate Programming</h2>
<ul>
<li>Build tools that help debug literate programs</li>
<li>Build tools that verify alignment of narrative with code</li>
<li>Faster tools for publishing to HTML</li>
<li>Build tools that allow multiple narratives of the same
code</li>
<li></li>

</ul>

</section>
</section>
<section>
<section id="slide-org831ed50">
<h2 id="org831ed50">Demo: Building literate Programs using Emacs Org-mode</h2>
<ul>
<li>Emacs: Basic Text editing and file management</li>

<li>Orgmode: Structured markup for creating narratives</li>

<li>Images and Tables</li>

</ul>

</section>
</section>
<section>
<section id="slide-org84a1085">
<h2 id="org84a1085">contd ..</h2>
<ul>
<li>Org-Babel: Tangling code in a narrative</li>

<li>Publishing: code and documentation in HTML</li>

</ul>

</section>
</section>
<section>
<section id="slide-orgb8681a3">
<h2 id="orgb8681a3">Questions</h2>
<p>
<code>venkatesh.choppella@iiit.ac.in</code>
</p>
</section>
</section>
</div>
</div>
<div class="postamble">2018-07-08 Sun 09:55</div>
<script src="lib-git-ignore/reveal.js/lib/js/head.min.js"></script>
<script src="lib-git-ignore/reveal.js/js/reveal.js"></script>

<script>
// Full list of configuration options available here:
// https://github.com/hakimel/reveal.js#configuration
Reveal.initialize({

controls: false,
progress: true,
history: false,
center: true,
slideNumber: 'c/t',
rollingLinks: false,
keyboard: true,
overview: true,
width: 1200,
height: 800,
margin: 0.10,
minScale: 0.50,
maxScale: 2.50,

theme: Reveal.getQueryHash().theme, // available themes are in /css/theme
transition: Reveal.getQueryHash().transition || 'fade*', // default/cube/page/concave/zoom/linear/fade/none
transitionSpeed: 'default',
multiplex: {
    secret: '', // null if client
    id: '', // id, obtained from socket.io server
    url: '' // Location of socket.io server
},

// Optional libraries used to extend on reveal.js
dependencies: [
{src: 'plugin/toc-progress/toc-progress.js', async: true, callback: function() { toc_progress.initialize(); toc_progress.create(); } },
 { src: 'lib-git-ignore/reveal.js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
 { src: 'lib-git-ignore/reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
 { src: 'lib-git-ignore/reveal.js/plugin/notes/notes.js', async: true, condition: function() { return !!document.body.classList; } },
]
});
</script>
</body>
</html>
